// =========================================================== //

#include <gokz/core>
#include <MovementHUD>

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo =
{
	name = "MovementHUD-Tracking-GOKZ",
	author = "Sikari",
	description = "Provides MHud tracking accuracy for GOKZ",
	version = "1.0.0",
	url = "https://bitbucket.org/Sikarii/movementhud-tracking"
};

// ======================= MAIN CODE ========================= //

public void MHud_Movement_OnTakeoff(int client, bool didJump, bool &didPerf, float &takeoffSpeed)
{	
	// Vanilla or unrecognized
	if (GOKZ_GetOption(client, gC_CoreOptionNames[Option_Mode]) <= Mode_Vanilla)
	{
		didPerf = MHud_Movement_GetGroundTicks(client) == 1;
		takeoffSpeed = MHud_Movement_GetCurrentSpeed(client);
	}
	else
	{
		didPerf = GOKZ_GetHitPerf(client);
		takeoffSpeed = GOKZ_GetTakeoffSpeed(client);
	}
}

// =========================================================== //