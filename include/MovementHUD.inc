// ================== DOUBLE INCLUDE ========================= //

#if defined _MovementHUD_included_
#endinput
#endif
#define _MovementHUD_included_

// ====================== DEFINITIONS ======================== //

#define MHUD_NAME "MovementHUD"
#define MHUD_AUTHOR "Sikari"
#define MHUD_VERSION "2.0.0"
#define MHUD_URL "https://bitbucket.org/Sikarii/MovementHUD"

#define MHUD_SETTINGS_REVISION 1
#define MHUD_TAG_RAW "[MovementHUD]"
#define MHUD_TAG_COLOR "[\x0CMovement\x07HUD\x01]"

#define EPSILON 0.000001
#define TEAM_SPECTATOR 1
#define SPEC_MODE_INEYE 4
#define SPEC_MODE_CHASE 5
#define INPUT_TIMEOUT 15.0

// =========================================================== //

#include <json>
#include <base64>
#include <clientprefs>
#include <StringMapEx>

#include <MovementHUD/preference>
#include <MovementHUD/preferences>
#include <MovementHUD/typehelpers>

// =========================================================== //

native Preference MHud_GetPreference(int iPref);
native Preference MHud_GetPreferenceByName(char[] name);

native int MHud_Movement_GetGroundTicks(int client);
native float MHud_Movement_GetCurrentSpeed(int client);

native void MHud_Print(int client, bool usePrefix, const char[] format, any ...);

// =========================================================== //

forward void MHud_OnSettingsLoaded(int client);
forward void MHud_OnSettingsImported(int client, PreferencesCode code);
forward void MHud_OnSettingsExported(int client, char[] code);

forward void MHud_OnExpectingInput(int client, Preference pref);
forward void MHud_OnInputCancelled(int client, Preference pref, bool timeout);

forward void MHud_OnPreferenceSet(int client, Preference pref, bool fromCommand);
forward void MHud_OnPreferenceCommand(int client, Preference pref, bool hadValue);

forward void MHud_Movement_OnTakeoff(int client, bool didJump, bool &didPerf, float &takeoffSpeed);

// ========================= STOCKS ========================== //

stock bool MHud_IsValidClient(int client, bool botsValid = false)
{
	bool valid = client >= 1 && client <= MaxClients && IsClientInGame(client);
	if (!botsValid)
	{
		valid = valid && !IsFakeClient(client);
	}

	return valid;
}

stock float GetSpeed(int client)
{
	float vel[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", vel);
	return SquareRoot(Pow(vel[0], 2.0) + Pow(vel[1], 2.0));
}

stock int GetSpectatorTarget(int client)
{
	if (GetClientTeam(client) == TEAM_SPECTATOR)
	{
		int mode = GetEntProp(client, Prop_Send, "m_iObserverMode");
		if (mode == SPEC_MODE_INEYE || mode == SPEC_MODE_CHASE)
		{
			return GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
		}
	}

	return client;
}

// ======================= PLUGIN INFO ======================= //

public SharedPlugin __pl_MovementHUD =
{
	name = "MovementHUD",
	file = "MovementHUD.smx",
	#if defined REQUIRE_PLUGIN
	required = 1,
	#else
	required = 0,
	#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_MovementHUD_SetNTVOptional()
{
	MarkNativeAsOptional("MHud_Print");
	MarkNativeAsOptional("MHud_GetPreference");
	MarkNativeAsOptional("MHud_GetPreferenceByName");
	MarkNativeAsOptional("MHud_Movement_GetGroundTicks");
	MarkNativeAsOptional("MHud_Movement_GetCurrentSpeed");
}
#endif

// =========================================================== //