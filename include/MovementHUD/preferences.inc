// ================== DOUBLE INCLUDE ========================= //

#if defined _MovementHUD_Preferences_included_
#endinput
#endif
#define _MovementHUD_Preferences_included_

// =========================================================== //

enum
{
	Pref_Speed_Display,
	Pref_Speed_Position,
	Pref_Speed_Normal_Color,
	Pref_Speed_Perf_Color,
	Pref_Keys_Display,
	Pref_Keys_Position,
	Pref_Keys_Normal_Color,
	Pref_Keys_Overlap_Color,
	PREF_COUNT
};

// =========================================================== //

static char FAILURE_TOKEN[] = "failure";
static char OWNER_TOKEN[] = "owner";
static char REVISION_TOKEN[] = "rev";

// =========================================================== //

methodmap PreferencesCode < StringMapEx
{
	public PreferencesCode()
	{
		StringMapEx codeObj = new StringMapEx();
		codeObj.SetValue(FAILURE_TOKEN, true);
		return view_as<PreferencesCode>(codeObj);
	}
	
	property int Owner
	{
		public get() { return this.GetInt(OWNER_TOKEN); }
		public set(int accountId) { this.SetValue(OWNER_TOKEN, accountId); }
	}
	
	property int Revision
	{
		public get() { return this.GetInt(REVISION_TOKEN); }
		public set(int revision) { this.SetValue(REVISION_TOKEN, revision); }
	}
	
	property bool Failure
	{
		public get() { return this.GetBool(FAILURE_TOKEN); }
		public set(bool failure) { this.SetValue(FAILURE_TOKEN, failure); }
	}

	public void GetSteamId2(char[] buffer, int maxlength)
	{
		int account = this.Owner / 2;
		if (account <= 0)
		{
			return;
		}

		int universe = this.Owner % 2 == 0 ? 0 : 1;
		Format(buffer, maxlength, "STEAM_1:%d:%d", universe, account);
	}
}

// =========================================================== //

methodmap Preferences < ArrayList
{
	public Preferences()
	{
		return view_as<Preferences>(new ArrayList());
	}
	
	public void PushPreference(Preference preference)
	{
		char name[MAX_PREFERENCE_NAME_LENGTH];
		preference.GetName(name, sizeof(name));
		
		this.Push(preference);
	}

	public Preference GetPreference(int pref)
	{
		return this.Get(pref);
	}
	
	public Preference GetPreferenceByName(char[] name)
	{
		for (int i = 0; i < this.Length; i++)
		{
			Preference pref = this.Get(i);

			char prefName[MAX_PREFERENCE_NAME_LENGTH];
			pref.GetName(prefName, sizeof(prefName));
			
			if (StrEqual(prefName, name))
			{
				return pref;
			}
		}
		
		return null;
	}
	
	public void ResetSettings(int client)
	{
		for (int i = 0; i < this.Length; i++)
		{
			this.GetPreference(i).Reset(client);
		}
	}
	
	public void GenerateSettingsCode(int client, char[] buffer, int maxlength)
	{
		JSON_Object settingsJson = new JSON_Object(false);
		
		settingsJson.SetInt(OWNER_TOKEN, GetSteamAccountID(client));
		settingsJson.SetInt(REVISION_TOKEN, MHUD_SETTINGS_REVISION);
		
		JSON_Object dataArr = new JSON_Object(true);
		for (int i = 0; i < this.Length; i++)
		{
			Preference pref = this.Get(i);
			char value[MAX_PREFERENCE_VALUE_LENGTH];
			pref.GetStringVal(client, value, sizeof(value));

			dataArr.PushString(value);
		}

		settingsJson.SetObject("data", dataArr);

		char js[256];
		settingsJson.Encode(js, sizeof(js));
		EncodeBase64(buffer, maxlength, js);

		settingsJson.Cleanup();
		delete settingsJson;
	}
	
	public PreferencesCode ImportSettingsFromCode(int client, char[] code)
	{
		PreferencesCode codeObj = new PreferencesCode();
		
		char buffer[256];
		DecodeBase64(buffer, sizeof(buffer), code);
		JSON_Object settingsJson = json_decode(buffer);

		if (settingsJson == null)
		{
			delete settingsJson;
			return codeObj;
		}

		codeObj.Owner = settingsJson.GetInt(OWNER_TOKEN);
		codeObj.Revision = settingsJson.GetInt(REVISION_TOKEN);

		if (codeObj.Revision == -1)
		{
			settingsJson.Cleanup();
			delete settingsJson;
			return codeObj;
		}

		JSON_Object dataArr = settingsJson.GetObject("data");
		if (dataArr == null)
		{
			settingsJson.Cleanup();
			delete settingsJson;
			return codeObj;
		}

		for (int i = 0; i < dataArr.Length; i++)
		{
			if (i < this.Length)
			{
				char value[MAX_PREFERENCE_VALUE_LENGTH];
				dataArr.GetStringIndexed(i, value, sizeof(value));

				this.GetPreference(i).SetVal(client, value);
			}
		}
		
		codeObj.Failure = false;
		settingsJson.Cleanup();
		delete settingsJson;
		return codeObj;
	}
}

// =========================================================== //